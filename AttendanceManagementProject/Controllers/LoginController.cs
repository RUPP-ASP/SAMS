﻿using AttendanceManagementProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AttendanceManagementProject.Controllers
{
    public class LoginController : Controller
    {

        private dbAttendanceEntity db = new dbAttendanceEntity();

        // GET: Login
        public ActionResult Index()
        {
            Session["Teacher"] = null;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "email,password")] Teacher teacher)
        {

            Teacher teach = null;
            foreach (Teacher t in db.Teachers) {
                if (t.email.Equals(teacher.email) && t.password.Equals(teacher.password)) {
                    teach = t;
                    break;
                }
            }

            if (teach==null) {
                ModelState.AddModelError("Error", "Check Email and Password Again");
            }

            if (ModelState.IsValid)
            {
                Session["Teacher"] = teach;
                return RedirectToAction("Create","Class");
            }

            return View(teacher);
        }
    }
}