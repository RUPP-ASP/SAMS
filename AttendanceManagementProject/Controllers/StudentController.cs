﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AttendanceManagementProject.Models;

namespace AttendanceManagementProject.Controllers
{
    public class StudentController : Controller
    {
        private dbAttendanceEntity db = new dbAttendanceEntity();

        // GET: Student
        public ActionResult Index()
        {

            
            int teacherId = ((Teacher)Session["Teacher"]).id;
            var getClasses = db.Classes.Where(c => c.teacher_id == teacherId);
            
            //.Select(c => new { id = c.id, textValue = string.Format("{0}-{1}-{2}", c.school_name, c.room_number, c.study_year) }).ToList();

            IEnumerable<SelectListItem> selectList = from c in getClasses
                                                     select new SelectListItem
                                                     {
                                                         Value = c.id+"",
                                                         Text = c.school_name + " / "+c.room_number +" / "+ c.study_year.ToString()
                                                     };

            ViewBag.classes = new SelectList(selectList, "Value", "Text");

            List<Student> students = new List<Student>();

            foreach (Class C in getClasses) {
                var stu = db.Students.Where(s => s.class_id == C.id);
                foreach (var myStu in stu) {
                    students.Add(myStu);
                }
            }
             

            return View(students.ToList());
        }

        // GET: Student/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // GET: Student/Create
        public ActionResult Create()
        {
            int teacherId = ((Teacher)Session["Teacher"]).id;
            ViewBag.class_id = new SelectList(db.Classes.Where(c => c.teacher_id == teacherId), "id", "school_name");
            return View();
        }

        // POST: Student/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,gender,dob,class_id")] Student student)
        {
            if (ModelState.IsValid)
            {
                db.Students.Add(student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.class_id = new SelectList(db.Classes, "id", "school_name", student.class_id);
            return View(student);
        }

        // GET: Student/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            int teacherId = ((Teacher)Session["Teacher"]).id;
            ViewBag.class_id = new SelectList(db.Classes.Where(c => c.teacher_id == teacherId), "id", "school_name", student.class_id);
            //ViewBag.class_id = new SelectList(db.Classes, "id", "school_name", student.class_id);
            return View(student);
        }

        // POST: Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,gender,dob,class_id")] Student student)
        {
            if (ModelState.IsValid)
            {
                db.Entry(student).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.class_id = new SelectList(db.Classes, "id", "school_name", student.class_id);
            return View(student);
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Student/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Student student = db.Students.Find(id);
            db.Students.Remove(student);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
