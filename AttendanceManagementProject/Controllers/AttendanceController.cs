﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AttendanceManagementProject.Models;

namespace AttendanceManagementProject.Controllers
{
    public class AttendanceController : Controller
    {
        private dbAttendanceEntity db = new dbAttendanceEntity();

        // GET: Attendance
        public ActionResult Index()
        {
            //var attendances = db.Attendances.Include(a => a.Class).Include(a => a.Student);


            int teacherId = ((Teacher)Session["Teacher"]).id;
            var getClasses = db.Classes.Where(c => c.teacher_id == teacherId);

            List<Attendance> attendances = new List<Attendance>();

            foreach (Class C in getClasses)
            {
                var stu = db.Attendances.Where(s => s.class_id == C.id);
                foreach (var myStu in stu)
                {
                    attendances.Add(myStu);
                }
            }


            return View(attendances.ToList());
        }

        // GET: Attendance/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendance attendance = db.Attendances.Find(id);
            if (attendance == null)
            {
                return HttpNotFound();
            }
            return View(attendance);
        }

        // GET: Attendance/Create
        public ActionResult Create()
        {

            int teacherId = ((Teacher)Session["Teacher"]).id;
            ViewBag.class_id = new SelectList(db.Classes.Where(c => c.teacher_id == teacherId), "id", "school_name");

            List<Student> students = new List<Student>();

            foreach (Class C in db.Classes.Where(c => c.teacher_id == teacherId))
            {
                var stu = db.Students.Where(s => s.class_id == C.id);
                foreach (var myStu in stu)
                {
                    students.Add(myStu);
                }
            }

            ViewBag.student_id = new SelectList(students, "id", "name");
            return View();
        }

        // POST: Attendance/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "class_id,student_id,date,status")] Attendance attendance)
        {
            if (ModelState.IsValid)
            {
                db.Attendances.Add(attendance);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.class_id = new SelectList(db.Classes, "id", "school_name", attendance.class_id);
            ViewBag.student_id = new SelectList(db.Students, "id", "name", attendance.student_id);
            return View(attendance);
        }

        // GET: Attendance/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendance attendance = db.Attendances.Find(id);
            if (attendance == null)
            {
                return HttpNotFound();
            }
            ViewBag.class_id = new SelectList(db.Classes, "id", "school_name", attendance.class_id);
            ViewBag.student_id = new SelectList(db.Students, "id", "name", attendance.student_id);
            return View(attendance);
        }

        // POST: Attendance/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "class_id,student_id,date,status")] Attendance attendance)
        {
            if (ModelState.IsValid)
            {
                db.Entry(attendance).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.class_id = new SelectList(db.Classes, "id", "school_name", attendance.class_id);
            ViewBag.student_id = new SelectList(db.Students, "id", "name", attendance.student_id);
            return View(attendance);
        }

        // GET: Attendance/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendance attendance = db.Attendances.Find(id);
            if (attendance == null)
            {
                return HttpNotFound();
            }
            return View(attendance);
        }

        // POST: Attendance/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Attendance attendance = db.Attendances.Find(id);
            db.Attendances.Remove(attendance);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
